import Vue from 'vue'
	import Router from 'vue-router'

    import MyCal from './views/MyCal.vue'
    import MyPin from './views/StickBoard.vue'
    import Home from './views/home.vue'
	import ToDo from './views/ToDo.vue'

	Vue.use(Router)
	export default new Router({
	  routes: [
		{
		  path: '/calendar',
		  name: 'Calendar',
		  component: MyCal
		},
		{
		  path: '/pinboard',
		  name: 'Pinboard',
		  component: MyPin
		},
        {
            path: '/',
            name: 'Home',
            component: Home
        },
		
		{
			path: '/todo',
            name: 'ToDo',
            component: ToDo
        } 
	  ]
	})